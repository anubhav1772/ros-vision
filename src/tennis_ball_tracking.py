#!/usr/bin/env python
import cv2
import numpy as np

vc = cv2.VideoCapture('video/tennis-ball-video.mp4')

while True:
    frame = vc.read()[1]
    
    hsv_img = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    yellow_Lower = (30, 120, 60)
    yellow_Upper = (60, 255, 255)
    mask = cv2.inRange(hsv_img, yellow_Lower, yellow_Upper)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    black_img = np.zeros([mask.shape[0], mask.shape[1], 3], 'uint8')
    
    for c in cnts:
        area = cv2.contourArea(c)
        perimeter = cv2.arcLength(c, True)
        ((x, y), radius) = cv2.minEnclosingCircle(c)

        if area>4000:
            M = cv2.moments(c)
            if M['m00']!=0:
                cx, cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
            
            cv2.drawContours(frame, [c], -1, (150, 250, 150), 2)
            cv2.circle(frame, (cx, cy), (int)(radius), (0, 0, 255), 2)

            cv2.drawContours(black_img, [c], -1, (150, 250, 150), 2)
            cv2.circle(black_img, (cx, cy), (int)(radius), (0, 0, 255), 2)
            cv2.circle(black_img, (cx,cy),5,(150,150,255),-1)
    
    cv2.imshow("Original Frame Contours", frame)
    cv2.imshow("Black Image Contours", black_img)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

vc.release()
cv2.destroyAllWindows()


