#!/usr/bin/env python
import rospy
import cv2
import sys
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

def image_callback(ros_image):
    global bridge
    try:
        cv_image = bridge.imgmsg_to_cv2(ros_image, "bgr8")
    except CvBridgeError as e:
        print(e)
    
    hsv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
    yellow_Lower = (30, 120, 60)
    yellow_Upper = (60, 255, 255)
    mask = cv2.inRange(hsv_img, yellow_Lower, yellow_Upper)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    black_img = np.zeros([mask.shape[0], mask.shape[1], 3], 'uint8')
    
    for c in cnts:
        area = cv2.contourArea(c)
        perimeter = cv2.arcLength(c, True)
        ((x, y), radius) = cv2.minEnclosingCircle(c)

        if area>4000:
            M = cv2.moments(c)
            if M['m00']!=0:
                cx, cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
            
            cv2.drawContours(cv_image, [c], -1, (150, 250, 150), 2)
            cv2.circle(cv_image, (cx, cy), (int)(radius), (0, 0, 255), 2)

            cv2.drawContours(black_img, [c], -1, (150, 250, 150), 2)
            cv2.circle(black_img, (cx, cy), (int)(radius), (0, 0, 255), 2)
            cv2.circle(black_img, (cx,cy),5,(150,150,255),-1)
 
    cv2.imshow("Original Frame Contours", cv_image)
    cv2.imshow("Black Image Contours", black_img)
    
    cv2.waitKey(0)
    


def main(args):
    rospy.init_node('subscriber_node', anonymous=True)
    image_topic = "/usb_cam/image_raw"
    image_sub = rospy.Subscriber(image_topic, Image, image_callback)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "shutting down"
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)