#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
# from cv_bridge import CvBridge, CvBridgeError



def publish_image_frame():
    # bridge = CvBridge()
    rospy.init_node('frame_publisher', anonymous=True)
    pub = rospy.Publisher('tennis_ball_image', Image, queue_size=100) 
    rate = rospy.Rate(30)

    vc = cv2.VideoCapture('video/tennis-ball-video.mp4')
    while not rospy.is_shutdown(): 
        while True:
            frame = vc.read()[1]
            # try:
            #     cv_image = bridge.imgmsg_to_cv2(frame, "bgr8")
            # except CvBridgeError as e:
            #     print(e)

            pub.publish(frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        vc.release()
        rate.sleep()

if __name__ == '__main__':
    try:
        publish_image_frame()
    except rospy.ROSInterruptException:
        pass
    




